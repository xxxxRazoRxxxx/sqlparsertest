fun main(args: Array<String>) {
    var query = "SELECT author.name, count(book.id), sum(book.cost) FROM (SELECT * FROM books, users), author RIGHT OUTER JOIN departments d ON u.d_id = d.id LEFT JOIN book ON (author.id = book.author_id)  GROUP BY author.name HAVING COUNT(*) > 1 AND SUM(book.cost) > 500 ORDER BY column1 ASC, column2 DESC, column3 ASC LIMIT 15 OFFSET 10 ROWS;"
    //var query = "SELECT d.id, d.name FROM users u RIGHT OUTER JOIN departments d ON u.d_id = d.id WHERE u.id IS null"

    var sqlQuery: sqlQuery? = null

    if(isQuery(query)){
        sqlQuery = sqlQuery(query)
    }

    println(sqlQuery!!.toString())
}

fun isQuery(Query: String): Boolean{
    return Query.toLowerCase().indexOf("select") >= 0 && Query.toLowerCase().indexOf("from") >= 0
}

class sqlQuery(Query: String){
    private var columns: List<String>? = null
    private var fromSources: List<Source>? =null
    private var joins: List<Join>? = null
    private var whereClauses: List<String>? = null
    private var groupByColumns: List<String>? = null
    private var sortColumns: List<Sort>? = null
    private var having: List<String>? = null
    private var limit: Int? = null
    private var offset: Int? = null

    init{
        var query = Query.toLowerCase().trim()
        columns = getSelectColumns(query)

        if(query.indexOf("offset") >= 0){
            offset = getOffset(query)
            query = query.substring(0, query.indexOf("offset")).trim()
        }

        if(query.indexOf("limit") >= 0){
            limit = getLimit(query)
            query = query.substring(0, query.indexOf("limit")).trim()
        }

        if(query.indexOf("order by") >= 0){
            sortColumns = getSort(query)
            query = query.substring(0, query.indexOf("order by")).trim()
        }

        if(query.indexOf("having") >= 0){
            having = getHaving(query)
            query = query.substring(0, query.indexOf("having")).trim()
        }

        if(query.indexOf("group by") >= 0){
            groupByColumns = getGroupBy(query)
            query = query.substring(0, query.indexOf("group by")).trim()
        }

        if(query.indexOf("where") >= 0){
            whereClauses = getwhereClauses(query)
            query = query.substring(0, query.indexOf("where")).trim()
        }

        fromSources = getFrom(query)

        query = query.substringAfter("from").substringAfter(fromSources!![fromSources!!.size-1].toString()).trim()

        if(query.indexOf("join") >= 0){
            joins = getJoins(query)
        }
    }

    fun getJoins(Query: String): List<Join>{
        val joinNum = (Query.split("join").size - 1);
        val result: MutableList<Join> = mutableListOf()
        var query = Query
        for(i in 1..joinNum){
            val joins = query.split("join")
            val _join = Join(joins[0], joins[1])
            result.add(_join)
            query = query.substringAfter(_join.getCondition()).trim()
        }
        return result
    }

    fun getFrom(query: String): List<Source>{
        val joinList = listOf("right", "left", "cross", "inner", "outer", "left outer", "right outer", "full outer")
        var fromString = query
        joinList.forEach{join ->
            if(query.indexOf(join) >= 0)
                if(query.split(join)[0].length < fromString.length)
                    fromString = query.split(join)[0]
        }
        var from: MutableList<Source> = mutableListOf()

        var index = fromString.indexOf("from")
        fromString = fromString.substring(index, fromString.length)

        val pattern = """,(?=(?:[^\)]|\([^\(]*\)*)*$)""".toRegex()
        val _from = pattern.split(fromString).filter { it.isNotBlank() }

        _from.forEach{
            from.add(Source(it))
        }
        return from
    }

    fun getwhereClauses(query: String): List<String>{
        return query.substringAfter("where").trim().split("and")
    }

    fun getSelectColumns(query: String): List<String>{
        return query.substringAfter("select").substringBefore("from").trim().split(",")
    }

    fun getOffset(query: String): Int{
        return query.substringAfter("offset").substringBefore("rows").trim().toInt()
    }

    fun getLimit(query: String): Int{
        return query.substringAfter("limit").trim().toInt()
    }

    fun getSort(query: String): List<Sort>{
        var result = mutableListOf<Sort>()
        var sortList = query.trim().substringAfter("order by").trim().split(",")
        sortList.forEach{
            result.add(Sort(it))
        }
        return result
    }

    fun getHaving(query: String): List<String>{
        return query.substringAfter("having").trim().split("and")
    }

    fun getGroupBy(query: String): List<String>{
        return query.substringAfter("group by").trim().split(",")
    }

    override fun toString(): String {
        var selectString = "SELECT ${columns!!.joinToString(",")}"
        var fromString = "FROM"
        var joinString = ""
        var havingString = ""
        var sortString = ""
        var limitString = ""
        var offsetString = ""
        var groupByString = ""
        var whereString = ""

        if(fromSources != null){
            val _froms = fromSources
            for(i in 0.._froms!!.size-1){
                fromString = "$fromString ${_froms[i].toString()}"
                if(i != _froms.size-1)
                    fromString = "$fromString,"
            }
        }

        if(groupByColumns != null)
            groupByString = " GROUP BY ${groupByColumns!!.joinToString(",")}"
        if(having != null)
            havingString = " HAVING ${having!!.joinToString(" AND ")}"
        if(offset != null)
            offsetString = " OFFSET $offset ROWS"
        if(limit != null)
            limitString = " LIMIT $limit"
        if(whereClauses != null)
            whereString = " WHERE ${whereClauses!!.joinToString(" AND ")}"
        if(sortColumns != null)
            sortString = " ORDER BY ${sortColumns!!.joinToString(",")}"
        if(joins != null){
            val _joins = joins
            for(i in _joins!!){
                joinString = "$joinString $i"
            }
        }

        val result = "$selectString $fromString $joinString$whereString$groupByString$havingString$sortString$limitString$offsetString"
        return result
    }
}

class Sort(Query: String){
    private var column = ""
    private var type = ""
    init{
        var (_column, _type) = Query.trim().split(' ')
        column = _column
        type = _type
    }

    override fun toString(): String {
        var result = "$column ${type.toUpperCase()}"
        return result
    }
}

class Source(Query: String){
    private var column = ""
    private var query: sqlQuery? = null
    init{
        if(isQuery(Query))
            query = sqlQuery(Query.substringAfter('(').substringBeforeLast(')'))
        else
            column = Query.trim()
    }

    override fun toString(): String {
        var result = if(query != null)
                        "(${query.toString()})"
                    else column
        return result
    }
}

class Join(Type: String, Condition: String){
    private var joinTypes = listOf("right", "left", "cross", "inner", "outer")
    private var type = ""
    private var table = ""
    private var condition = ""

    init{
        type = Type.trim()
        var (_table, _condition) = Condition.split("on")
        table = _table.trim()
        joinTypes.forEach{
            _condition = _condition.substringBefore(it)
        }
        condition = _condition.trim()
    }

    fun getCondition():String{
        return condition
    }

    override fun toString(): String {
        val joinString = "${type.toUpperCase()} JOIN $table on $condition"
        return joinString
    }
}